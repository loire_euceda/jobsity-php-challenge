import '../css/tweet_show.scss';

import $ from 'jquery';
import Swal from 'sweetalert2/dist/sweetalert2.js';

const routes = require('../../public/js/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';

Routing.setRoutingData(routes);

$(document).ready(function(){
    $(".show-hide-tweet").on('click', function(event){
        var tweetId = $(this).attr("data-tweet-id");
        var action = $(this).attr("data-action");
        var route = '';

        if (action === "hide") {
        	var route = 'setHideTweetState';
        	var anchorText = 'Show tweet';
        	var newAction = 'show';
            var confirmationText = 'The tweet was hidden';
        } else if (action === "show") {
        	var route = 'setShowTweetState';
        	var anchorText = 'Hide tweet';
        	var newAction = "hide";
            var confirmationText = 'The tweet is now visible';
        }

        if (route) {
        	$.ajax({
    	        type: "POST",
    	        url: Routing.generate(route),
    	        data: { tweetId : tweetId },
    	        success: function(data) {
    	        	var element = '*[data-tweet-id="' + tweetId + '"]';
    			    $('*[data-tweet-content-id="' + tweetId + '"]').toggleClass("hidden-tweet");
                    $(element).toggleClass("badge-warning badge-secondary text-body");
    			    $(element).text(anchorText);
    			    $(element).attr("data-action", newAction); 

                    Swal.fire({
                      position: 'top-end',
                      icon: 'success',
                      title: confirmationText,
                      showConfirmButton: false,
                      timer: 2500,
                      toast: true
                    });
    	        }
    	    });
        }
    });
});
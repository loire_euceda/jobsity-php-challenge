<?php

namespace App\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SluggerTest extends WebTestCase
{
    public function testParamSlug()
    {
        self::bootKernel();

        $container = self::$kernel->getContainer();
        $container = self::$container;

        $this->assertEquals('steve-jobs',
            self::$container->get('App\Service\Slugger')->slugify('Steve Jobs')
        );
    }
}
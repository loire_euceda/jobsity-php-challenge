<?php

namespace App\Tests\Service;

use App\Model\TweetSource;
use App\Service\JsonFileSource;
use App\Service\TwitterApiSource;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TweetSrvTest extends WebTestCase
{
    public function login($email, $password)
    {
        self::ensureKernelShutdown();
        $client = static::createClient();

        $crawler = $client->request('GET', '/login');
        $this->assertEquals('App\Controller\SecurityController::login', $client->getRequest()->attributes->get('_controller'));

        $form = $crawler->selectButton('Log in')->form([
            'email' => $email,
            'password' => $password
        ]);

        $client->submit($form);
    }

    public function testTweetOwner()
    {
        self::bootKernel();

        $container = self::$kernel->getContainer();
        $this->login('loire.euceda@gmail.com', 'holamundo');
        $tweetSource = new TweetSource();
        /*$tweetSource->addData(New JsonFileSource(
            '/../../info',
            'TwitterAPI.json'
        ));*/
        $tweetSource->addData(New TwitterApiSource(
            self::$container->get('App\Service\TwitterOauthClient'),
            sprintf($_ENV['TIMELINE_USER'], 'TwitterAPI', 1),
            'TwitterAPI'
        ));
        foreach (self::$container->get('App\Service\TweetSrv')->getTweetsContent($tweetSource, 1) as $val) {
            $this->assertEquals(true, $val['owner']);
        }
    }

    public function testGetTweetsContent()
    {
        self::bootKernel();

        $container = self::$kernel->getContainer();
        $tweetSource = new TweetSource();
        /*$tweetSource->addData(New JsonFileSource(
            '/../../info',
            'TwitterAPI.json'
        ));*/
        $tweetSource->addData(New TwitterApiSource(
            self::$container->get('App\Service\TwitterOauthClient'),
            sprintf($_ENV['TIMELINE_USER'], 'TwitterAPI', 1),
            'TwitterAPI'
        ));
        foreach (self::$container->get('App\Service\TweetSrv')->getTweetsContent($tweetSource, 1) as $val) {
            $this->assertArrayHasKey('owner',
                $val
            );
        }
    }

    public function testDeleteNonExistentHiddenTweet()
    {
        self::bootKernel();

        $container = self::$kernel->getContainer();
        $this->login('loire.euceda@gmail.com', 'holamundo');

        $this->assertFalse(self::$container->get('App\Service\TweetSrv')->deleteHiddenTweet('1'));
    }

    public function testDeleteHiddenTweet()
    {
        self::bootKernel();

        $container = self::$kernel->getContainer();
        $this->login('loire.euceda@gmail.com', 'holamundo');

        $this->assertTrue(self::$container->get('App\Service\TweetSrv')->deleteHiddenTweet('1174362863118372867'));
    }

    public function testDeleteAnotherUserHiddenTweet()
    {
        self::bootKernel();

        $container = self::$kernel->getContainer();
        $this->login('fabpot@gmail.com', 'holamundo');

        $this->assertFalse(self::$container->get('App\Service\TweetSrv')->deleteHiddenTweet('1174362863118372867'));
    }

    public function testCreateHiddenTweet()
    {
        self::bootKernel();

        $container = self::$kernel->getContainer();
        $this->login('loire.euceda@gmail.com', 'holamundo');

        $this->assertTrue(self::$container->get('App\Service\TweetSrv')->createHiddenTweet('1214281438092238855'));
    }

    public function testCreateHiddenTweetWithDifferentOwner()
    {
        self::bootKernel();

        $container = self::$kernel->getContainer();
        $this->login('fabpot@gmail.com', 'holamundo');

        $this->assertFalse(self::$container->get('App\Service\TweetSrv')->createHiddenTweet('1214281438092238855'));
    }
}
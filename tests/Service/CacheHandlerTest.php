<?php

namespace App\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CacheHandlerTest extends WebTestCase
{
    public function testDataIsCachedAndAccessible()
    {
        self::bootKernel();

        $container = self::$kernel->getContainer();

        $cacheHandler = self::$container->get('App\Service\CacheHandler');

        $cacheHandler->saveCacheData('test', 'Hello World');

        $this->assertTrue($cacheHandler->isInCacheData('test'));

        $this->assertEquals('Hello World', $cacheHandler->getCachedData('test'));

    }
}
<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;


class HomeControllerTest extends WebTestCase
{
    public function testHomepage()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', '/');

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testMinusZeroPaginationLimit()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', '/-1');

        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }

    public function testOnlyNumbersPaginationLimit()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', '/qwe');

        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }
}
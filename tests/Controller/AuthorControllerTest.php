<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class AuthorControllerTest extends WebTestCase
{
    public function testAuthorHomepage()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', 'author/loire-euceda');

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testMinusZeroPaginationLimit()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', 'author/loire-euceda/-1');

        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }

    public function testOnlyNumbersPaginationLimit()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', 'author/loire-euceda/qwe');

        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }

    public function testAuthorNotFound()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', 'author/not-existent-author');

        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }

    public function testShowUserWithTweets()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', 'author/loire-euceda/tweets');

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testShowUserWithoutTweets()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', 'author/lorem-ipsum5/tweets');

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }
}
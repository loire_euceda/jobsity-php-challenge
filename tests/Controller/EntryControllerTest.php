<?php

namespace App\Tests\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\BrowserKit\Cookie;

class EntryControllerTest extends WebTestCase
{
    protected function createAuthorizedClient()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $container = static::$kernel->getContainer();
        $session = $container->get('session');
        $user = self::$kernel->getContainer()
            ->get('doctrine')
            ->getRepository(User::class)
            ->findOneBy([
                'email' => 'loire.euceda@gmail.com'
            ]);

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();

        $client->getCookieJar()->set(new Cookie($session->getName(), $session->getId()));

        return $client;
    }

    public function testEntryEditPage()
    {
        $client = $this->createAuthorizedClient();
        $client->request('GET', 'entry/edit/lorem-ipsum-10-lorem-ipsum');
        $this->assertEquals(
            Response::HTTP_OK,
            $client->getResponse()->getStatusCode()
        );
    }

    public function testEntryNotFound()
    {
        $client = $this->createAuthorizedClient();
        $client->request('GET', 'entry/edit/nero-the-cat-3');

        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }
    
    public function testEditUnauthorizedEntry()
    {
        $client = $this->createAuthorizedClient();
        $client->request('GET', 'entry/edit/lorem-ipsum-5-lorem-ipsum');

        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function testEditEntry()
    {
        $client = $this->createAuthorizedClient();
        $crawler = $client->request('GET', 'entry/edit/lorem-ipsum-10-lorem-ipsum');

        $form = $crawler->selectButton('Save')->form([
            'entry[title]'   => 'lorem.ipsumlorem.ipsum21313',
            'entry[content]' => 'lorem.ipsumipsumipsumipsumipsumipsumipsum',
        ]);

        $client->submit($form);
        $client->followRedirect();

        $this->assertContains(
            'Entry successfully updated!',
            $client->getResponse()->getContent()
        );
    }

    public function testNewEntry()
    {
        $client = $this->createAuthorizedClient();
        $crawler = $client->request('GET', 'entry/new');

        $form = $crawler->selectButton('Create')->form([
            'entry[title]'   => 'lorem.ipsumlorem.ipsum21313',
            'entry[content]' => 'lorem.ipsumipsumipsumipsumipsumipsumipsum',
        ]);

        $client->submit($form);
        $client->followRedirect();

        $this->assertContains(
            'Entry successfully created!',
            $client->getResponse()->getContent()
        );
    }
}
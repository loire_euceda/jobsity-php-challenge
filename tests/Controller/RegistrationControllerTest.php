<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class RegistrationControllerTest extends WebTestCase
{
    public function testShowRegister()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', '/register');

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testRegistration()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();

        $crawler = $client->request('GET', '/register');
        $this->assertEquals('App\Controller\RegistrationController::register', $client->getRequest()->attributes->get('_controller'));

        $form = $crawler->selectButton('Register')->form([
            'registration[username]'           => 'lorem.ipsum',
            'registration[twitterUsername]'    => 'lorem.ipsum',
            'registration[email]'              => 'lorem.ipsum@gmail.com',
            'registration[password][first]'    => 'NeroTheCat',
            'registration[password][second]'   => 'NeroTheCat'
        ]);

        $client->submit($form);
        $client->followRedirect();

        $this->assertEquals('App\Controller\HomeController::homepage', $client->getRequest()->attributes->get('_controller'));
        $this->assertContains(
            'You have successfully registered!',
            $client->getResponse()->getContent()
        );
    }
}
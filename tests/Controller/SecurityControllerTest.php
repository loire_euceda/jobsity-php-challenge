<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{
    public function testShowLogin()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', '/login');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testLogin()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();

        $crawler = $client->request('GET', '/login');
        $this->assertEquals('App\Controller\SecurityController::login', $client->getRequest()->attributes->get('_controller'));

        $form = $crawler->selectButton('Log in')->form([
            'email' => 'loire.euceda@gmail.com',
            'password' => 'holamundo'
        ]);

        $client->submit($form);

        $client->followRedirect();
        $this->assertEquals('App\Controller\HomeController::homepage', $client->getRequest()->attributes->get('_controller'));
    }
}
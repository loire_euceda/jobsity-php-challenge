-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-02-2020 a las 21:13:21
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `aliciatest`
--
CREATE DATABASE IF NOT EXISTS `aliciatest` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `aliciatest`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entry`
--

CREATE TABLE `entry` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `entry`
--

INSERT INTO `entry` (`id`, `author_id`, `title`, `content`, `creation_date`, `updated_at`, `slug`) VALUES
(161, 116, 'Lorem Ipsum 0 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:00', '2020-02-28 20:46:41', 'lorem-ipsum-0-lorem-ipsum'),
(162, 116, 'Lorem Ipsum 1 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:01', '2020-02-28 20:46:41', 'lorem-ipsum-1-lorem-ipsum'),
(163, 116, 'Lorem Ipsum 2 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:02', '2020-02-28 20:46:41', 'lorem-ipsum-2-lorem-ipsum'),
(164, 116, 'Lorem Ipsum 3 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:03', '2020-02-28 20:46:41', 'lorem-ipsum-3-lorem-ipsum'),
(165, 116, 'Lorem Ipsum 4 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:04', '2020-02-28 20:46:41', 'lorem-ipsum-4-lorem-ipsum'),
(166, 116, 'Lorem Ipsum 5 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:05', '2020-02-28 20:46:41', 'lorem-ipsum-5-lorem-ipsum'),
(167, 111, 'Lorem Ipsum 6 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:06', '2020-02-28 20:46:41', 'lorem-ipsum-6-lorem-ipsum'),
(168, 112, 'Lorem Ipsum 7 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:07', '2020-02-28 20:46:41', 'lorem-ipsum-7-lorem-ipsum'),
(169, 113, 'Lorem Ipsum 8 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:08', '2020-02-28 20:46:41', 'lorem-ipsum-8-lorem-ipsum'),
(170, 114, 'Lorem Ipsum 9 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:09', '2020-02-28 20:46:41', 'lorem-ipsum-9-lorem-ipsum'),
(171, 115, 'Lorem Ipsum 10 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:10', '2020-02-28 20:46:41', 'lorem-ipsum-10-lorem-ipsum'),
(172, 115, 'Lorem Ipsum 11 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:11', '2020-02-28 20:46:41', 'lorem-ipsum-11-lorem-ipsum'),
(173, 115, 'Lorem Ipsum 12 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:12', '2020-02-28 20:46:41', 'lorem-ipsum-12-lorem-ipsum'),
(174, 115, 'Lorem Ipsum 13 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:13', '2020-02-28 20:46:41', 'lorem-ipsum-13-lorem-ipsum'),
(175, 115, 'Lorem Ipsum 14 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:14', '2020-02-28 20:46:41', 'lorem-ipsum-14-lorem-ipsum'),
(176, 115, 'Lorem Ipsum 15 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:15', '2020-02-28 20:46:41', 'lorem-ipsum-15-lorem-ipsum'),
(177, 115, 'Lorem Ipsum 16 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:16', '2020-02-28 20:46:41', 'lorem-ipsum-16-lorem-ipsum'),
(178, 115, 'Lorem Ipsum 17 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:17', '2020-02-28 20:46:41', 'lorem-ipsum-17-lorem-ipsum'),
(179, 115, 'Lorem Ipsum 18 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:18', '2020-02-28 20:46:41', 'lorem-ipsum-18-lorem-ipsum'),
(180, 115, 'Lorem Ipsum 19 Lorem Ipsum', '\r\n            <p><img alt=\'\' src=\'https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600\' style=\'height:247px; width:800px\' /></p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>\r\n        ', '2020-02-24 12:49:19', '2020-02-28 20:46:41', 'lorem-ipsum-19-lorem-ipsum');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hidden_tweet`
--

CREATE TABLE `hidden_tweet` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `source_tweet_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `hidden_tweet`
--

INSERT INTO `hidden_tweet` (`id`, `author_id`, `source_tweet_id`) VALUES
(94, 115, '1174362863118372867');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20200221222617', '2020-02-23 04:29:02'),
('20200221224703', '2020-02-23 04:29:02'),
('20200222052822', '2020-02-23 04:29:02'),
('20200222065415', '2020-02-23 04:29:02'),
('20200222071302', '2020-02-23 04:29:02'),
('20200222082750', '2020-02-23 04:29:02'),
('20200222090331', '2020-02-23 04:29:02'),
('20200222222846', '2020-02-23 04:29:02'),
('20200223111403', '2020-02-24 23:25:08'),
('20200223111824', '2020-02-24 23:25:08'),
('20200223191205', '2020-02-24 23:25:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`roles`)),
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter_username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registered_at` datetime NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `username`, `twitter_username`, `registered_at`, `slug`) VALUES
(105, 'lorem.ipsum0@gmail.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$YnRsUUxBdjR3OE9rLlhNVw$oOpn+jYQb5OFRhbnEez/uIpRPAUH0C5onN0GcpNgmOI', 'lorem-ipsum0', 'lorem_ipsum0', '2020-02-28 20:46:38', 'lorem-ipsum0'),
(106, 'lorem.ipsum1@gmail.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$NFhNblhzcTdncjJaOER0dg$5A4+JLnjzpJzLxxI+jQZP5s0hppqIo5eHw+k1vVjfDM', 'lorem-ipsum1', 'lorem_ipsum1', '2020-02-28 20:46:38', 'lorem-ipsum1'),
(107, 'lorem.ipsum2@gmail.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$aW5ZQWhxcWlLN2VYeDFuMQ$HIWst8Xw3XYQBAGjLSPsP50ktG15hX1bZYHFoxzM0JI', 'lorem-ipsum2', 'lorem_ipsum2', '2020-02-28 20:46:38', 'lorem-ipsum2'),
(108, 'lorem.ipsum3@gmail.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$bzYuOExrb1hFNTZERUc5dw$fHTqEDJJmCvPc07Mv5QSDM0f2gEp9KBfvrsqOskD+kk', 'lorem-ipsum3', 'lorem_ipsum3', '2020-02-28 20:46:39', 'lorem-ipsum3'),
(109, 'lorem.ipsum4@gmail.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$V3lLenl5UmVBSWd4Rmpaeg$f1+sOs8ojc4SYvNFTgudRGB5ogUBqnCuskpWIpZ92cw', 'lorem-ipsum4', 'lorem_ipsum4', '2020-02-28 20:46:39', 'lorem-ipsum4'),
(110, 'lorem.ipsum5@gmail.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$R0VpZzJUWWVMQnpBT1Vjdg$xaOafVjKyhlcvRv2PNES+87lObXWS0zYK9xFR2P+E2Y', 'lorem-ipsum5', 'lorem_ipsum5', '2020-02-28 20:46:39', 'lorem-ipsum5'),
(111, 'lorem.ipsum6@gmail.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$VnZrWHFZZHRJTFAuakt5Uw$9D05xscWTyKr++WIuW1HSQlBAq4Kk1P/y/2qRdZ31x8', 'lorem-ipsum6', 'lorem_ipsum6', '2020-02-28 20:46:39', 'lorem-ipsum6'),
(112, 'lorem.ipsum7@gmail.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$c1RwQmtPRXV2VEs4UG81OQ$Oeu+W39C4aKWoi4GhibnpQj48gNI3a3o+T3H958+e9Q', 'lorem-ipsum7', 'lorem_ipsum7', '2020-02-28 20:46:40', 'lorem-ipsum7'),
(113, 'lorem.ipsum8@gmail.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$V0xMVndmSG5OLnpUeUtWNA$BgSnL0frHxvsG4OCpHE3jqJC9AYtS0x9DRU8DlOi5XE', 'lorem-ipsum8', 'lorem_ipsum8', '2020-02-28 20:46:40', 'lorem-ipsum8'),
(114, 'lorem.ipsum9@gmail.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$RmIvRVhSZUtHUDBzZUhFZQ$DTenquMj2nUhC7+eHWPaPteEed5hXPJfdlqUfbYD+vk', 'lorem-ipsum9', 'lorem_ipsum9', '2020-02-28 20:46:40', 'lorem-ipsum9'),
(115, 'loire.euceda@gmail.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$bzdWeTRLNVNIdUFadzBicA$kUMkHiltxMD8eXuzYjOOmUBliaEKLcruGlpYsWSEklg', 'loire-euceda', 'TwitterAPI', '2020-02-28 20:46:41', 'loire-euceda'),
(116, 'fabpot@gmail.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$a05KVGRFQjk3SUJyTEVjbQ$DhSeR9r1HGnFDqUfMrHfr5/2OTGfqONr0kx4LzWv4IQ', 'fabpot', 'fabpot', '2020-02-28 20:46:41', 'fabpot');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `entry`
--
ALTER TABLE `entry`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_2B219D702B36786B` (`title`),
  ADD KEY `IDX_2B219D70F675F31B` (`author_id`);

--
-- Indices de la tabla `hidden_tweet`
--
ALTER TABLE `hidden_tweet`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_E28FDEE55361D549` (`source_tweet_id`),
  ADD KEY `IDX_E28FDEE5F675F31B` (`author_id`);

--
-- Indices de la tabla `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_8D93D649E69385EB` (`twitter_username`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `entry`
--
ALTER TABLE `entry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;

--
-- AUTO_INCREMENT de la tabla `hidden_tweet`
--
ALTER TABLE `hidden_tweet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `entry`
--
ALTER TABLE `entry`
  ADD CONSTRAINT `FK_2B219D70F675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `hidden_tweet`
--
ALTER TABLE `hidden_tweet`
  ADD CONSTRAINT `FK_E28FDEE5F675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

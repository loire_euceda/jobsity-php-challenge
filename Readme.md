## Jobsity's PHP Challenge by Alicia Euceda

This application is developed using Symfony 4.4.

## Requirements
- php ^7.1.3
- composer
- mysql
- Redis
- Apache

## Clone the project

Go to the following directory and clone the repository
```bash
cd /var/www/jobsitychallenge/
git clone https://loire_euceda@bitbucket.org/loire_euceda/jobsity-php-challenge.git alicia_euceda
```
## Configure Apache

Create the vhost file for Apache
```bash
sudo nano /etc/apache2/sites-available/alicia_euceda.jobsitychallenge.conf
```
Put the following information on it
```bash
<VirtualHost *:80>
    ServerName alicia_euceda.jobsitychallenge.com
	
	DocumentRoot "/var/www/jobsitychallenge/alicia_euceda/public"
	<Directory "/var/www/jobsitychallenge/alicia_euceda/public">
	    AllowOverride All
	    Order allow,deny
	    Allow from all
	</Directory>

    ErrorLog /var/log/apache2/project_error.log
    CustomLog /var/log/apache2/project_access.log combined
</VirtualHost>
```

Enabled the site 
```bash
sudo a2ensite alicia_euceda.jobsitychallenge.conf
```
Restart Apache to make these changes take effect
```bash
sudo service apache2 restart
```

Set up a local host file
```bash
sudo nano /etc/hosts
```

Add the following line
```bash
127.0.0.1   alicia_euceda.jobsitychallenge.com
```

## Install Redis
For detailed instruction:
```bash
https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-18-04
```

## Setup the Web App
Open the .env file located on the root folder of the project and adjust the following parameter to your needs
```bash
DATABASE_URL=mysql://root:root@127.0.0.1:3306/alicia-challenge?serverVersion=8.0
REDIS_HOST=localhost
REDIS_PORT=6379
```

And set Twitter's credentials
```bash
###> twitter credentials ###
CONSUMER_KEY="your_consumer_key"
CONSUMER_SECRET="your_consumer_secret"
TOKEN="your_token"
TOKEN_SECRET="your_token_secret"
###< twitter credentials ###
```

Install the vendors
```bash
composer install 
```

## Configure the database
You can load the db script located at DB/ or follow next steps:

Create the database runing the following command
```bash
php bin/console doctrine:database:create
```

Create the tables
```bash
php bin/console doctrine:migrations:migrate
```
Load the data fixtures
```bash
php bin/console doctrine:fixtures:load
```

## Project's setup is ready

Now everything must be ready, click on the following link to begin

http://alicia_euceda.jobsitychallenge.com

There are 2 user set with linked Twitter account
```bash
username = loire.euceda@gmail.com
password = holamundo

username = fabpot@gmail.com
password = holamundo
```

## Run the tests
For testing you must load the data fixture or db script first, is this is done before, just run the following command
```bash
php bin/phpunit
```
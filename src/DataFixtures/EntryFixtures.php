<?php

namespace App\DataFixtures;

use App\Entity\Entry;
use App\Service\Slugger;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class EntryFixtures extends Fixture implements DependentFixtureInterface
{
    private $slugger;

    public function __construct(Slugger $slugger)
    {
        $this->slugger = $slugger;
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            $entry = new Entry();
            $entry->setTitle("Lorem Ipsum ".$i." Lorem Ipsum");
            $entry->setContent($this->getContentText());
            if($i < 6) {
                $entry->setAuthor($this->getReference(UserFixtures::AUTHOR_REFERENCE_FABPOT));
            } elseif($i > 5 && $i < 10) {
                $entry->setAuthor($this->getReference(UserFixtures::AUTHOR_REFERENCE.$i));
            } else {
                $entry->setAuthor($this->getReference(UserFixtures::AUTHOR_REFERENCE));
            }
            $entry->setSlug($this->slugger->slugify("Lorem Ipsum ".$i." Lorem Ipsum"));
            $entry->setCreationDate(new \DateTime(date('2020-02-24 12:49:'.$i)));

            $manager->persist($entry);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }

    public function getContentText()
    {
        return "
            <p><img alt='' src='https://images.pexels.com/photos/2832043/pexels-photo-2832043.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=650&amp;w=600' style='height:247px; width:800px' /></p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>
        ";
    }
}
<?php

namespace App\DataFixtures;

use App\Entity\Tweet;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class TweetFixture extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $tweet = new Tweet();
        $tweet->setSourceTweetId('1174362863118372867');
        $tweet->setAuthor($this->getReference(UserFixtures::AUTHOR_REFERENCE));

        $manager->persist($tweet);
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }
}
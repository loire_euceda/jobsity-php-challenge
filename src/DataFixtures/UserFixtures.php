<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Service\Slugger;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public const AUTHOR_REFERENCE = 'author-user';
    public const AUTHOR_REFERENCE_FABPOT = 'author-fabpot';
    private $passwordEncoder;
    private $slugger;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, Slugger $slugger)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->slugger = $slugger;
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setEmail("lorem.ipsum".$i."@gmail.com");
            $user->setUsername("lorem-ipsum".$i);
            $user->setTwitterUsername("lorem_ipsum".$i);
            $user->setSlug($this->slugger->slugify("lorem-ipsum".$i));
            $user->setRoles(['ROLE_USER']);
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'holamundo'
            ));
            $manager->persist($user);
            $this->addReference(self::AUTHOR_REFERENCE.$i, $user);
        }

        $user = new User();
        $user->setEmail("loire.euceda@gmail.com");
        $user->setUsername("loire-euceda");
        $user->setTwitterUsername("TwitterAPI");
        $user->setSlug($this->slugger->slugify("loire-euceda"));
        $user->setRoles(['ROLE_USER']);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'holamundo'
        ));
        $manager->persist($user);
        $this->addReference(self::AUTHOR_REFERENCE, $user);

        $user = new User();
        $user->setEmail("fabpot@gmail.com");
        $user->setUsername("fabpot");
        $user->setTwitterUsername("fabpot");
        $user->setSlug($this->slugger->slugify("fabpot"));
        $user->setRoles(['ROLE_USER']);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'holamundo'
        ));
        $manager->persist($user);
        $this->addReference(self::AUTHOR_REFERENCE_FABPOT, $user);

        $manager->flush();
    }
}

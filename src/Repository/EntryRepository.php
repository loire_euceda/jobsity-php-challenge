<?php

namespace App\Repository;

use App\Entity\Entry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Entry|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entry|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entry[]    findAll()
 * @method Entry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EntryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Entry::class);
    }

    /**
     * @param int $currentPage
     * @param int $limit
     * @return Paginator
     */
    public function getAll($currentPage = 1, $limit = 3)
    {
        $query = $this
            ->createQueryBuilder('entry')
            ->select('
                entry.slug as entrySlug, 
                entry.title, 
                entry.content, 
                entry.creationDate, 
                user.username, 
                user.email,
                user.slug as userSlug'
            )
            ->innerJoin('entry.author', 'user')
            ->orderBy('entry.creationDate', 'DESC')
            ->getQuery();

        return $paginator = $this->paginate($query, $currentPage, $limit);
    }

    /**
     * @param $userSlug
     * @param int $currentPage
     * @param int $limit
     * @return Paginator
     */
    public function getAllByUser($userSlug, $currentPage = 1, $limit = 5)
    {
        $query = $this
            ->createQueryBuilder('entry')
            ->select('
                entry.slug as entrySlug, 
                entry.title, 
                entry.content, 
                entry.creationDate, 
                user.username,
                user.twitterUsername, 
                user.email,
                user.slug as userSlug'
            )
            ->innerJoin('entry.author', 'user')
            ->where('user.slug = :userSlug')
            ->setParameter('userSlug', $userSlug)
            ->orderBy('entry.creationDate', 'DESC')
            ->getQuery();

        return $paginator = $this->paginate($query, $currentPage, $limit);
    }

    public function findOneBySlug($slug): ?Entry
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.slug = :val')
            ->setParameter('val', $slug)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function paginate($dql, $page = 1, $limit = 3)
    {
        $paginator = new Paginator($dql);
        $paginator->setUseOutputWalkers(false);
        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1)) // Offset
            ->setMaxResults($limit); // Limit

        return $paginator;
    }
}

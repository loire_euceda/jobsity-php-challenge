<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TweetRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="hidden_tweet")
 */
class Tweet
{
    const LIMIT_PAG = 5;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $sourceTweetId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tweets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSourceTweetId(): ?int
    {
        return $this->sourceTweetId;
    }

    public function setSourceTweetId(int $sourceTweetId): self
    {
        $this->sourceTweetId = $sourceTweetId;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }
}

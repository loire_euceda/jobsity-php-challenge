<?php

namespace App\Controller;

use App\Entity\Entry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/{currentPage}", name="homepage", requirements={"currentPage"="\d+"})
     * @param int $currentPage
     * @return Response
     */
    public function homepage($currentPage = 1): Response
    {
        $em = $this->getDoctrine()->getManager();
        $entries = $em->getRepository(Entry::class)->getAll($currentPage, Entry::LIMIT_PAG);

        return $this->render('homepage/index.html.twig', [
            'entries'   => $entries,
            'maxPages'  => ceil($entries->count() / Entry::LIMIT_PAG),
            'thisPage'  => $currentPage,
        ]);
    }
}
<?php

namespace App\Controller;

use App\Entity\Entry;
use App\Entity\Tweet;
use App\Entity\User;
use App\Model\TweetSource;
use App\Service\TweetSrv;
use App\Service\TwitterApiSource;
use App\Service\TwitterOauthClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class AuthorController extends AbstractController
{
    /**
     * @Route("author/{userSlug}/{currentPage}", name="authorHomepage", requirements={"currentPage"="\d+"})
     * @ParamConverter("user", options={"mapping"={"userSlug"="slug"}})
     * @param $userSlug
     * @param int $currentPage
     * @return Response
     */
    public function homepage(User $user, $currentPage = 1): Response
    {   
        if (!$user) {
            throw $this->createNotFoundException('Author not found');
        }

        $em = $this->getDoctrine()->getManager();
        $entries = $em->getRepository(Entry::class)->getAllByUser($user->getSlug(), $currentPage, User::LIMIT_PAG);

        return $this->render('homepage/author.html.twig', [
            'entriesCount'  => $entries->count(),
            'entries'       => $entries,
            'userSlug'      => $user->getSlug(),
            'maxPages'      => ceil($entries->count() / User::LIMIT_PAG),
            'thisPage'      => $currentPage,
        ]);
    }

    /**
     * @Route("author/{userSlug}/tweets", name="authorTweets")
     * @ParamConverter("user", options={"mapping"={"userSlug"="slug"}})
     * @param $userSlug
     * @param TweetSrv $tweetSrv
     * @param TwitterOauthClient $twitterClient
     * @return Response
     */
    public function showTweets(User $user, TweetSrv $tweetSrv, TwitterOauthClient $twitterClient): Response
    {
        $tweetSource = new TweetSource();
        $tweetSource->addData(New TwitterApiSource(
            $twitterClient,
            sprintf($_ENV['TIMELINE_USER'], $user->getTwitterUsername(), Tweet::LIMIT_PAG),
            $user->getTwitterUsername()
        ));

        if ($tweetSource->getData() != null) {
            $tweets = $tweetSrv->getTweets($tweetSource, Tweet::LIMIT_PAG);
        }

        if (empty($tweets)) {
            return $this->render('homepage/tweets.html.twig');
        }

        return $this->render('homepage/tweets.html.twig', [
            'tweets'   => $tweets,
        ]);
    }

    /**
     * @Route("tweets/state/hide", options={"expose"=true}, name="setHideTweetState")
     * @param Request $request
     * @param TweetSrv $tweetSrv
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ErrorException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function hideTweet(Request $request, TweetSrv $tweetSrv): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $this->denyAccessUnlessGranted('ROLE_USER', null, 'Access denied');

        if ($tweetSrv->createHiddenTweet($_POST['tweetId'])) {
            return new JsonResponse("Tweet successfully hidden");
        }

        return new JsonResponse("Access denied");
    }

    /**
     * @Route("tweets/state/show", options={"expose"=true}, name="setShowTweetState")
     * @param Request $request
     * @param TweetSrv $tweetSrv
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteHiddenTweet(Request $request, TweetSrv $tweetSrv): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $this->denyAccessUnlessGranted('ROLE_USER', null, 'Access denied');

        if ($tweetSrv->deleteHiddenTweet($_POST['tweetId'])) {
            return new JsonResponse("Tweet successfully show");
        }

        return new JsonResponse("Access denied");
    }
}
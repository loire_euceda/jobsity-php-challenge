<?php

namespace App\Controller;

use App\Entity\Entry;
use App\Form\EntryType;
use App\Service\Slugger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class EntryController extends AbstractController
{
    /**
     * @Route("/entry/new", name="newEntry")
     * @param Request $request
     * @param Slugger $slugger
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function new(Request $request, Slugger $slugger, ValidatorInterface $validator) : Response
    {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();

        $entry = new Entry();
        $form = $this->createForm(EntryType::class, $entry);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entry->setAuthor($user);
            $entry->setSlug($slugger->slugify($form->get('title')->getData()));

            $errors = $validator->validate($entry);
            if (count($errors) > 0) {
                return new Response((string) $errors, Response::HTTP_BAD_REQUEST);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entry);
            $entityManager->flush();

            $this->addFlash('success', 'Entry successfully created!');
            return $this->redirect($this->generateUrl('homepage'));
        }

        return $this->render('entry/new.html.twig', [
            'entryForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/entry/edit/{slug}", name="editEntry")
     * @ParamConverter("entry", options={"mapping"={"slug"="slug"}})
     * @param Request $request
     * @param Entry $entry
     * @param Slugger $slugger
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function edit(Request $request, Entry $entry, Slugger $slugger, ValidatorInterface $validator) : Response
    {
        if($this->getUser() !== $entry->getAuthor()) {
            throw $this->createAccessDeniedException('You do not have permissions to edit this entry');
        }

        $form = $this->createForm(EntryType::class, $entry);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entry->setSlug($slugger->slugify($form->get('title')->getData()));

            $errors = $validator->validate($entry);
            if (count($errors) > 0) {
                return new Response((string) $errors, Response::HTTP_BAD_REQUEST);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            $this->addFlash('success', 'Entry successfully updated!');
            return $this->redirect($this->generateUrl('homepage'));
        }

        return $this->render('entry/edit.html.twig', [
            'entryForm' => $form->createView(),
        ]);
    }
}
<?php

namespace App\Interfaces;


interface TweetSourceInterface
{
    public function getContent();
}
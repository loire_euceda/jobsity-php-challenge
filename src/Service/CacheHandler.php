<?php

namespace App\Service;

use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\InvalidArgumentException;

class CacheHandler
{
    /**
     * @var CacheItemPoolInterface
     */
    private $cache;

    public function __construct(CacheItemPoolInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param string $key
     * @return \Psr\Cache\CacheItemInterface
     * @throws InvalidArgumentException
     */
    public function getCachedData(string $key)
    {
        return $this->cache->getItem(md5($key))->get();
    }

    /**
     * @param string $key
     * @return bool
     * @throws InvalidArgumentException
     */
    public function isInCacheData(string $key)
    {
        $cachedItem = $this->cache->getItem(md5($key));

        return $cachedItem->isHit();
    }

    /**
     * @param string $key
     * @param $data
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function saveCacheData(string $key, $data)
    {
        $cacheKey = md5($key);
        $cachedItem = $this->cache->getItem($cacheKey);
        $cachedItem->set($data);
        $this->cache->save($cachedItem);

        return $cachedItem->get();
    }
}
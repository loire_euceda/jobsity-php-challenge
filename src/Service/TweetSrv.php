<?php

namespace App\Service;

use App\Entity\Tweet;
use App\Model\TweetSource;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\EntityManager;

class TweetSrv
{
    private $security;
    private $em;
    private $twitterClient;
    private $hashtagUrl;
    private $twitterAccountUrl;
    private $showTweetEndpoint;

    public function __construct(
        EntityManager $em,
        Security $security,
        TwitterOauthClient $twitterClient,
        $hashtagUrl,
        $twitterAccountUrl,
        $showTweetEndpoint
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->twitterClient = $twitterClient;
        $this->hashtagUrl = $hashtagUrl;
        $this->twitterAccountUrl = $twitterAccountUrl;
        $this->showTweetEndpoint = $showTweetEndpoint;
    }

    /**
     * @param TweetSource $tweetSource
     * @param $limit
     * @return array
     */
    public function getTweets(TweetSource $tweetSource, $limit): array
    {
        $tweets = $this->getTweetsContent($tweetSource, $limit);
        $hiddenTweets = $this->em->getRepository(Tweet::class)->getAll();

        foreach ($tweets as $key => $tweet) {
            $tweets[$key]['hidden'] = false;
            foreach ($hiddenTweets as $hiddenTweet) {
                if ($tweet['id'] == $hiddenTweet['sourceTweetId']) {
                    $tweets[$key]['hidden'] = true;
                    break;
                }
            }
        }

        return $tweets;
    }

    /**
     * @param TweetSource $tweetSource
     * @param $limit
     * @return array
     */
    public function getTweetsContent(TweetSource $tweetSource, $limit): array
    {
        $user = $this->security->getUser();
        $tweets = [];

        foreach ($tweetSource->getData() as $data) {
            if (count($tweets) < $limit) {
                $isOwner = false;
                $fullText = $data['full_text'];
                $retweeted = false;
                $retweetedInfo = [];

                if ($user != null) {
                    if ($data['user']['screen_name'] == $user->getTwitterUsername()) {
                        $isOwner = true;
                    }
                }

                if (array_key_exists('retweeted_status', $data)) {
                    $retweeted = true;
                    $fullText = $data['retweeted_status']['full_text'];

                    $retweetedInfo = [
                        'name' => $data['retweeted_status']['user']['name'],
                        'screen_name' => $data['retweeted_status']['user']['screen_name'],
                        'verified' => $data['retweeted_status']['user']['verified'],
                        'favorite_count' => $data['retweeted_status']['favorite_count'],
                        'retweet_count' => $data['retweeted_status']['retweet_count']
                    ];
                }

                $fullText = $this->getMarkedText($fullText);

                array_push($tweets, [
                    'id'    => $data['id_str'],
                    'text'  => $fullText,
                    'owner' => $isOwner,
                    'profile' => [
                        'name' => $data['user']['name'],
                        'screen_name' => $data['user']['screen_name'],
                        'location' => $data['user']['location'],
                        'background_color' => $data['user']['profile_background_color'],
                        'background_image' => $data['user']['profile_background_image_url_https'],
                        'profile_image' => $data['user']['profile_image_url_https'],
                        'verified' => $data['user']['verified']
                    ],
                    'favorite_count' => $data['favorite_count'],
                    'retweet_count' => $data['retweet_count'],
                    'retweeted' => $retweeted,
                    'retweeted_info' => $retweetedInfo
                ]);
            } else {
                break;
            }
        }

        return $tweets;
    }

    /**
     * @param $fullText
     * @return string
     */
    public function getMarkedText($fullText): string
    {
        $fullText = preg_replace(
            '/(^|\s)#(\w*[a-zA-Z_]+\w*)/',
            '\1<a href="'.$this->hashtagUrl.'\2" target="_blank">#\2</a>',
            $fullText
        );

        $fullText = preg_replace(
            '/(^|\s)@(\w*[a-zA-Z_]+\w*)/',
            '\1<a href="'.$this->twitterAccountUrl.'\2" target="_blank">@\2</a>',
            $fullText
        );

        $fullText = preg_replace(
            "/(?<!a href=\")(?<!src=\")((http|ftp)+(s)?:\/\/[^<>\s]+)/i",
            "<a href=\"\\0\" target=\"blank\">\\0</a>",
            $fullText
        );

        return $fullText;
    }

    /**
     * @param $tweetId
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ErrorException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function createHiddenTweet($tweetId)
    {
        $currentUser = $this->security->getUser();

        $data = $this->twitterClient->getRequest(
            sprintf($this->showTweetEndpoint, $tweetId),
            $tweetId
        );

        if ($data['user']['screen_name'] == $currentUser->getTwitterUsername()) {
            $tweet = new Tweet();
            $tweet->setAuthor($currentUser);
            $tweet->setSourceTweetId($tweetId);

            $this->em->persist($tweet);
            $this->em->flush();

            return true;
        }

        return false;
    }

    /**
     * @param $tweetId
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteHiddenTweet($tweetId)
    {
        $currentUser = $this->security->getUser();
        $tweet = $this->em->getRepository(Tweet::class)->findOneBy([
            'sourceTweetId' => $tweetId
        ]);

        if ($tweet != null) {
            if($tweet->getAuthor() === $currentUser) {
                $this->em->remove($tweet);
                $this->em->flush();
                return true;
            }
        }

        return false;
    }
}
<?php

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Symfony\Component\HttpFoundation\Response;

class TwitterOauthClient
{
    private $cacheHandler;
    private $baseUrl;
    private $consumerKey;
    private $consumerSecret;
    private $token;
    private $tokenSecret;

    public function __construct(
        CacheHandler $cacheHandler,
        $baseUrl,
        $consumerKey,
        $consumerSecret,
        $token,
        $tokenSecret
    ) {
        $this->cacheHandler = $cacheHandler;
        $this->baseUrl = $baseUrl;
        $this->consumerKey = $consumerKey;
        $this->consumerSecret = $consumerSecret;
        $this->token = $token;
        $this->tokenSecret = $tokenSecret;
    }

    /**
     * @param $endpoint
     * @param $key
     * @return array
     * @throws \ErrorException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getRequest($endpoint, $key): array
    {
        if($this->cacheHandler->isInCacheData($key) === true) {
            return json_decode($this->cacheHandler->getCachedData($key), true);
        }

        $client = new Client([
            'base_url' => $this->baseUrl,
            'defaults' => [
                'auth' => 'oauth',
                'exceptions' => false
            ]
        ]);
        $oauth = new Oauth1([
            'consumer_key'    => $this->consumerKey,
            'consumer_secret' => $this->consumerSecret,
            'token'           => $this->token,
            'token_secret'    => $this->tokenSecret
        ]);
        $client->getEmitter()->attach($oauth);
        $response = $client->get($endpoint);
        $statusCode = $response->getStatusCode();

        if (Response::HTTP_OK === $statusCode) {
            $data = $client->get($endpoint)->json();
            $this->cacheHandler->saveCacheData($key, json_encode($data));

            return $data;
        }
        elseif (Response::HTTP_NOT_MODIFIED === $statusCode) {
            return [];
        }
        elseif (Response::HTTP_NOT_FOUND === $statusCode) {
            return [];
        } elseif (Response::HTTP_UNAUTHORIZED === $statusCode) {
            return [];
        }elseif (Response::HTTP_FORBIDDEN === $statusCode) {
            return [];
        } else {
            throw new \ErrorException('Invalid response from api');
        }
    }
}
<?php

namespace App\Service;

use App\Interfaces\TweetSourceInterface;

class TwitterApiSource implements TweetSourceInterface
{
    private $twitterClient;
    private $endpoint;
    private $key;

    public function __construct(TwitterOauthClient $twitterClient, $endpoint, $key)
    {
        $this->twitterClient = $twitterClient;
        $this->endpoint = $endpoint;
        $this->key = $key;
    }

    /**
     * @return mixed
     * @throws \ErrorException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getContent()
    {
        return $this->twitterClient->getRequest($this->endpoint, $this->key);
    }
}
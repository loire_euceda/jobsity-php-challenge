<?php

namespace App\Service;

use App\Interfaces\TweetSourceInterface;
use Symfony\Component\Finder\Finder;

class JsonFileSource implements TweetSourceInterface
{
    private $directory;
    private $fileName;

    /**
     * FileSourceData constructor.
     * @param $directory
     * @param $fileName
     */
    public function __construct($directory, $fileName)
    {
        $this->directory = $directory;
        $this->fileName = $fileName;
    }

    /**
     * @param $fileName
     * @param $directory
     * @return string
     */
    public function findFile($directory, $fileName)
    {
        $finder = new Finder();
        $finder->files()->in(__DIR__ . $directory);
        $content = '';

        if ($finder->hasResults()) {
            foreach ($finder as $file) {
                $fileNameWithExtension = $file->getRelativePathname();
                if ($fileNameWithExtension == $fileName) {
                    $content = $file->getContents();
                    break;
                }
            }
        }
        return $content;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $data = json_decode($this->findFile($this->directory, $this->fileName), true);
    }
}
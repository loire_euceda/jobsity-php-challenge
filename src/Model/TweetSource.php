<?php

namespace App\Model;


use App\Interfaces\TweetSourceInterface;


class TweetSource
{
    Protected $data = [];

    /**
     * @param TweetSourceInterface $source
     */
    public function addData(TweetSourceInterface $source)
    {
        $this->data = $source->getContent();
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}